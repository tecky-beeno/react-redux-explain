# FRD Extra - Explain The Magic Behind Redux

video part 1: https://youtu.be/nkg9RKqwNvs

video part 2: https://youtu.be/S8s0NeFM-dQ

demo code: https://gitlab.com/tecky-beeno/react-redux-explain

This may help you understand roughly how redux and react-redux works behind the scene but don't feel stressed if you cannot get it. Because we only need to work with their surface APIs in normal cases.
