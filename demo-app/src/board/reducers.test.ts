import { initState } from './state';
import { nextStep, reset } from './actions';
import { boardReducers } from './reducers';
import { networkInterfaces } from 'os';

describe('Board Reducer TestSuit', () => {
  it('should have empty board as init state', function () {
    expect(initState).toBeDefined();
    expect(initState.error).toBeUndefined();
    expect(initState.oIsNext).toBe(true);
    let nineNull: null[] = [];
    for (let i = 0; i < 9; i++) {
      nineNull.push(null);
    }
    expect(initState.squares).toEqual(nineNull);
  });

  it('should perform next step', function () {
    let nextState = boardReducers(initState, nextStep('O', 0));
    expect(nextState).toBeDefined();
    expect(nextState.error).toBeUndefined();
    expect(nextState.oIsNext).toBe(false);
    let expectedSquares = initState.squares.slice();
    expectedSquares[0] = 'O';
    expect(nextState.squares).toEqual(expectedSquares);
  });

  it("should reject non-current round player's move", function () {
    let nextState = boardReducers(initState, nextStep('X', 0));
    expect(nextState).toBeDefined();
    expect(nextState.error).toBeDefined();
    expect(nextState.oIsNext).toBe(true);
    expect(nextState.squares).toEqual(initState.squares);
  });

  it('should reject step on occupied square', function () {
    let round1 = boardReducers(initState, nextStep('O', 0));
    let round2 = boardReducers(round1, nextStep('X', 1));
    expect(round2).toBeDefined();
    expect(round2.error).toBeUndefined();
    expect(round2.oIsNext).toBe(true);
    let expectedSquares = initState.squares.slice();
    expectedSquares[0] = 'O';
    expectedSquares[1] = 'X';
    expect(round2.squares).toEqual(expectedSquares);
  });

  it('should reset', function () {
    let round1 = boardReducers(initState, nextStep('O', 0));
    let round2 = boardReducers(round1, reset());
    expect(round2).toEqual(initState);
  });
});
