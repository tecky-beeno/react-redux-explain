export function nextStep(player: string, index: number) {
  return {
    type: 'NEXT_STEP' as 'NEXT_STEP',
    player,
    index,
  };
}

export function reset() {
  return {
    type: 'RESET' as 'RESET',
  };
}

export type GameBoardAction =
  | ReturnType<typeof nextStep>
  | ReturnType<typeof reset>;
