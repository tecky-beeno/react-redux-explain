import { IBoardState, initState } from './state';
import { GameBoardAction } from './actions';
import { checkWinner } from '../Game';

export function boardReducers(
  state: IBoardState = initState,
  action: GameBoardAction,
): IBoardState {
  switch (action.type) {
    case 'NEXT_STEP': {
      if (state.oIsNext && action.player !== 'O') {
        return { ...state, error: 'Current Round is for Player O' };
      }
      if (!state.oIsNext && action.player !== 'X') {
        return { ...state, error: 'Current Round is for Player X' };
      }
      if (checkWinner(state.squares)) {
        return { ...state, error: 'Cannot move after Game Over' };
      }
      if (state.squares[action.index]) {
        return { ...state, error: 'Cannot place on non-empty square' };
      }
      let squares = state.squares.slice();
      squares[action.index] = action.player;
      return {
        ...state,
        oIsNext: !state.oIsNext,
        error: undefined,
        squares,
      };
    }
    case 'RESET':
      return initState;
    default:
      return state;
  }
}
