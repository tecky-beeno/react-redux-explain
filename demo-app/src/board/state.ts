export type IBoardState = {
  squares: Array<string | null>;
  oIsNext: boolean;
  error?: string;
};
export let initState: IBoardState = {
  squares: new Array(9).fill(null),
  oIsNext: true,
};
