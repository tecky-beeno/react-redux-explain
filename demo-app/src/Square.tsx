import React from 'react';
import './Square.css';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from './store';
import { nextStep } from './board/actions';

function Square(props: { index: number; winning: boolean }) {
  let dispatch = useDispatch();
  let playerInSquare = useSelector(
    (state: RootState) => state.board.squares[props.index] || '_',
  );
  let nextPlayer = useSelector((state: RootState) =>
    state.board.oIsNext ? 'O' : 'X',
  );
  let winningClass = props.winning ? 'winning' : '';

  function click() {
    dispatch(nextStep(nextPlayer, props.index));
  }

  return (
    <button className={'square ' + winningClass} onClick={click}>
      {playerInSquare}
    </button>
  );
}

export default Square;
