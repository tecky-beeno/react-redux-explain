import React, { Component, useState } from 'react';
import { Provider } from 'react-redux';
import Board from './Board';
import './App.css';
import store from './store';
import Score from './Score';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="game">
          <Board />
          <Score />
          <div className="game-info">
            <div>{/* status */}</div>
            <ol>{/* TODO */}</ol>
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;

// Provider({store},[div({className:'game'},[])])
