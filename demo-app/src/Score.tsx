import React from 'react';
import { RootStateOrAny, useSelector } from 'react-redux';
import { RootState } from './store';

function cell(x: string | null) {
  switch (x) {
    case 'X':
    case 'O':
      return x;
    default:
      return '_';
  }
}

function range(n: number) {
  let res: number[] = [];
  for (let i = 0; i < n; i++) {
    res.push(i);
  }
  return res;
}

function Score() {
  let scores = useSelector((state: RootState) => state.score.scores);
  return (
    <table className="Score">
      <thead>
        <tr>
          <th>name</th>
          <th>winner</th>
          <th>squares</th>
        </tr>
      </thead>
      <tbody>
        {scores.map((score) => (
          <tr>
            <td>{score.name}</td>
            <td>{score.winner}</td>
            <td style={{ border: '1px solid black', padding: '8px' }}>
              {range(3).map((row) => (
                <div>
                  {range(3).map((col) => cell(score.squares[row * 3 + col]))}
                </div>
              ))}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default Score;
