import React, { useMemo, useState } from 'react';
import Square from './Square';
import './Board.css';
import { checkWinner, Size, toIdx } from './Game';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from './store';
import { addWinner } from './score/actions';

export default function Board() {
  const dispatch = useDispatch();
  const squares = useSelector((state: RootState) => state.board.squares);
  const nextPlayer = useSelector((state: RootState) =>
    state.board.oIsNext ? 'O' : 'X',
  );
  const errorMessage = useSelector((state: RootState) => state.board.error);
  const [lastWinner, setLastWinner] = useState<any>(undefined);
  let winner = checkWinner(squares);
  useMemo(() => {
    if (!lastWinner && winner) {
      setLastWinner(winner);
      let name = prompt('Your name') || 'Player';
      dispatch(addWinner(name, squares, winner.player));
    }
  }, [lastWinner, winner]);

  let rows: any[] = [];
  let i = 0;
  for (let row = 0; row < Size; row++) {
    let cols: any[] = [];
    for (let col = 0; col < Size; col++) {
      let isWinningSquare =
        winner?.winningLine.some((rowCol) => toIdx(rowCol) === i) ?? false;
      cols.push(<Square winning={isWinningSquare} key={i} index={i} />);
      i++;
    }
    rows.push(
      <div className="row" key={row}>
        {cols}
      </div>,
    );
  }
  return (
    <div className="Board">
      <div className="status">
        {winner ? `Winner: ${winner.player}` : `Next Player: ${nextPlayer}`}
      </div>
      {rows}
      <div className="error">{errorMessage}</div>
    </div>
  );
}
