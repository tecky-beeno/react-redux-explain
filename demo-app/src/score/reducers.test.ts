import { addWinner } from './actions';
import { KEY, scoreReducers } from './reducers';

it('should have init state', function () {
  localStorage.removeItem(KEY);
  let initState = scoreReducers(undefined, {} as any);
  expect(initState).toBeDefined();
  expect(initState.scores).toHaveLength(0);
});

let squares = [null, 'X', 'X', null, null, null, 'O', 'O', 'O'];
it('should add winner', function () {
  localStorage.removeItem(KEY);
  let initState = scoreReducers(undefined, { type: '@@INIT' } as any);
  let state = scoreReducers(initState, addWinner('Alice', squares, 'O'));
  expect(state).toBeDefined();
  expect(state.scores).toHaveLength(1);
  expect(state.scores[0].name).toBe('Alice');
  expect(state.scores[0].winner).toBe('O');
  expect(state.scores[0].squares).toEqual(squares);
});

it('should load state from localStorage', function () {
  let state = scoreReducers(undefined, {} as any);
  expect(state).toBeDefined();
  expect(state.scores).toHaveLength(1);
  expect(state.scores[0].name).toBe('Alice');
  expect(state.scores[0].winner).toBe('O');
  expect(state.scores[0].squares).toEqual(squares);
});
