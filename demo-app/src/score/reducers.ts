import { IScoreState } from './state';
import { ScoreBoardAction } from './actions';

export let KEY = 'scores';

function initState(): IScoreState {
  try {
    let scores = JSON.parse(localStorage.getItem('scores') || '[]');
    if (scores) {
      return { scores };
    }
  } catch (e) {}
  return { scores: [] };
}

export function scoreReducers(
  state: IScoreState | undefined,
  action: ScoreBoardAction,
): IScoreState {
  if (!state) {
    return initState();
  }
  switch (action.type) {
    case 'ADD_WINNER': {
      let newState: IScoreState = {
        ...state,
        scores: [
          ...state.scores,
          {
            squares: action.squares,
            winner: action.winner,
            name: action.name,
          },
        ],
      };
      localStorage.setItem(KEY, JSON.stringify(newState.scores));
      return newState;
    }
    default:
      return state;
  }
}
