export function addWinner(
  name: string,
  squares: Array<string | null>,
  winner: string,
) {
  return {
    type: 'ADD_WINNER' as 'ADD_WINNER',
    name,
    squares,
    winner,
  };
}

export type ScoreBoardAction = ReturnType<typeof addWinner>;
