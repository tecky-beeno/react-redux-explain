import {createStore, combineReducers} from "redux";
import {scoreReducers} from "./score/reducers";
import {boardReducers} from "./board/reducers";
import {GameBoardAction} from "./board/actions";
import {ScoreBoardAction} from "./score/actions";

let store = createStore(combineReducers({
    board: boardReducers,
    score: scoreReducers,
}))
export type RootState = ReturnType<typeof store.getState>
export type IAction = GameBoardAction | ScoreBoardAction
export default store

store.getState()

store.dispatch

store.subscribe()
