// import React, {useState} from "react";
import React from 'react';
import ReactDOM from 'react-dom';
import { Store } from 'redux';

class Component {
  context: Context = new Context(this);
  parent?: Component;
  props: any;

  render() {
    return <>To be overridden</>;
  }
}

class Context {
  hostElement!: HTMLElement;

  constructor(public component: Component) {}

  update() {
    let vdom = this.component.render();
    ReactDOM.render(vdom, this.hostElement);
  }

  allState: any[] = [];

  createState(initState, index) {
    let object = {
      value: initState,
      update: (value) => {
        object.value = value;
        this.update();
      },
    };
    this.allState[index] = object;
    return object;
  }

  stateCount = 0;

  getStateIndex() {
    this.stateCount++;
    return this.stateCount;
  }
}

function useState(initState: any) {
  let index = this.context.getStateIndex();
  if (!this.context.firstTime) {
    return this.context.restoreState(index);
  }
  let state = this.context.createState(initState, index);

  function setState(value: any) {
    state.update(value);
  }

  return [state.value, setState];
}

// if called outside a react component, will fail to look up current context
// let [c, setC] = useState(1);

export function UseStateFC() {
  let [counter, setCounter] = useState(1);
  let [counter1, setCounter1] = useState(1);
  let [counter2, setCounter2] = useState(1);
  return <div onClick={() => setCounter(counter + 1)}>{counter}</div>;
}

export function App() {
  return (
    <div>
      <UseStateFC />
      <UseStateFC />
    </div>
  );
}

export function FCToClass(fc: any) {
  return class extends React.Component<any, any> {
    render() {
      return fc();
    }
  };
}

export function Provider(props: { store: Store }, children: any[]) {
  return children;
}

export function useSelector(selector: (state: any) => any) {
  let component = this.context.component;
  for (;;) {
    if (component.props.store) {
      break;
    }
    let parent = component.parent;
    if (!parent) {
      throw new Error('Did you forget to provide the store in a Provider?');
    }
    component = parent;
  }
  let store = component.props.store;
  let state = store.getState();
  let initialSelectedState = selector(state);
  let [selectedState, setSelectedState] = useState(initialSelectedState);
  store.subscribe(() => {
    let newState = store.getState();
    let newSubState = selector(newState);
    setSelectedState(newSubState);
  });
  return selectedState;
}

export function useDispatch(selector: (state: any) => any) {
  let component = this.context.component;
  for (;;) {
    if (component.props.store) {
      break;
    }
    let parent = component.parent;
    if (!parent) {
      throw new Error('Did you forget to provide the store in a Provider?');
    }
    component = parent;
  }
  let store = component.props.store;
  return store.dispatch;
}

export function createStore(reducer: any) {
  let state = reducer(undefined, { type: '@@INIT' });
  let listeners: Array<() => void> = [];

  function getState() {
    return state;
  }

  function dispatch(action: any) {
    let newState = reducer(state, action);
    state = newState;
    listeners.forEach((listener) => listener());
  }

  function subscribe(listener: () => void) {
    listeners.push(listener);
  }

  let store = {
    getState,
    dispatch,
    subscribe,
  };
  return store;
}

export type Reducer<S, A> = (state: S, action: A) => S;

export function combineReducer(
  reducers: Reducer<any, any>[],
): Reducer<any, any> {
  return function reducer(state: any, action: any) {
    Object.entries(reducers).forEach((entry) => {
      let name = entry[0];
      let reducer = entry[1];
      let subState = state[name];
      let newSubState = reducer(subState, action);
      let newState = {
        ...state,
        [name]: newSubState,
      };
      state = newState;
    });
    return state;
  };
}
