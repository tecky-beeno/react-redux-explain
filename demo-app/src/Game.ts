export const Size = 3;
export const Players = ['X', 'O'];
export const WinningLines: Line[] = [];

// The Cross Lines
let line: Line = [];
for (let i = 0; i < Size; i++) {
  line.push({ row: i, col: i });
}
WinningLines.push(line);
line = [];
for (let i = 0; i < Size; i++) {
  line.push({ row: i, col: Size - 1 - i });
}
WinningLines.push(line);

// The Horizontal Lines
for (let row = 0; row < Size; row++) {
  line = [];
  for (let col = 0; col < Size; col++) {
    line.push({ row, col });
  }
  WinningLines.push(line);
}

// The Vertical Lines
for (let col = 0; col < Size; col++) {
  line = [];
  for (let row = 0; row < Size; row++) {
    line.push({ row, col });
  }
  WinningLines.push(line);
}



type Line = RowCol[];
type RowCol = { row: number; col: number };

export function toIdx({ col, row }: RowCol) {
  return row * Size + col;
}

export function checkWinner(squares: Array<string|null>) {
  for (let player of Players) {
    for (let winningLine of WinningLines) {
      let allOccupied = winningLine.every((rowCol) => {
        let idx = toIdx(rowCol);
        return squares[idx] === player;
      });
      if (allOccupied) {
        return { player, winningLine };
      }
    }
  }
}
