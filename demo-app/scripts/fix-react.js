let fs = require('fs')
let data = require('../tsconfig.json')
data.compilerOptions.jsx = 'react'
let text = JSON.stringify(data, null, 2) + '\n'
fs.writeFileSync('tsconfig.json', text)
